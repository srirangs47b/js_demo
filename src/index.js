import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'antd/dist/antd.css';
import { ApolloClient, ApolloProvider, InMemoryCache } from '@apollo/client';
import { createStore } from 'redux';
import { Provider } from 'react-redux'
import  rootReducer  from './reducers/index'
// import incrementDecrement from './reducers/incrementDecrement'


const store = createStore(rootReducer);


export const client = new ApolloClient({
  uri: 'https://48p1r2roz4.sse.codesandbox.io',
  cache: new InMemoryCache()
});


ReactDOM.render(
 
    <ApolloProvider  client = {client}>
    <Provider store = {store}>
    <App />
    </Provider>
    </ApolloProvider>,
 
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
