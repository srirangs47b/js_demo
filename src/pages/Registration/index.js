import React from 'react';
import { Link } from 'react-router-dom';
import { Form, FormGroup, Card, CardBody, Container } from 'reactstrap';


const Registration = () => {
    return (
        <Container className="registration-container mt-5">
            <Card>
                <CardBody>
                <Form>
                <h3 className="text-center">Sign Up</h3>

                <FormGroup className="form-group">
                    <label>First name</label>
                    <input type="text" className="form-control" placeholder="First name" />
                </FormGroup>

                <FormGroup className="form-group">
                    <label>Last name</label>
                    <input type="text" className="form-control" placeholder="Last name" />
                </FormGroup>

                <FormGroup className="form-group">
                    <label>Email address</label>
                    <input type="email" className="form-control" placeholder="Enter email" />
                </FormGroup>

                <FormGroup className="form-group">
                    <label>Password</label>
                    <input type="password" className="form-control" placeholder="Enter password" />
                </FormGroup>

                <button type="submit" className="btn btn-primary btn-block">Sign Up</button>
                <p className="forgot-password text-right">
                    Already registered <Link to="/login">Log In</Link>
                </p>
            </Form>
                </CardBody>
            </Card>
        </Container>
    )
}

export default Registration