import React from 'react';
import { Form, FormGroup,  Card, CardBody, Container, Row, Col } from 'reactstrap';
import { Link } from 'react-router-dom';


const Login = () => {
    return (
        <Container className="login-container mt-5">
            <Card>
                <CardBody>
                    <Form>
                        <h3 className="text-center">Log In</h3>

                        <FormGroup className="form-group">
                            <label>Email address</label>
                            <input type="email" className="form-control" placeholder="Enter email" />
                        </FormGroup>

                        <FormGroup className="form-group">
                            <label>Password</label>
                            <input type="password" className="form-control" placeholder="Enter password" />
                        </FormGroup>

                        <FormGroup className="form-group">
                            <div className="custom-control custom-checkbox">
                                <input type="checkbox" className="custom-control-input" id="customCheck1" />
                                <label className="custom-control-label" htmlFor="customCheck1">Remember me</label>
                            </div>
                        </FormGroup>

                        <button type="submit" className="btn btn-primary btn-block">Submit</button>
                        <Row>
                            <Col className="md-6">
                            <p className="forgot-password text-left">Not yet registered <Link to="/registration">Register Now </Link></p>
                            </Col>
                           <Col className="md-6">
                           <p className="forgot-password text-right">
                            Forgot <a href="#">password?</a>
                        </p>
                           </Col>
                           </Row>
                    </Form>
                </CardBody>
            </Card>
        </Container>
    )
}

export default Login