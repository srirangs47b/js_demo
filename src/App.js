// import logo from './logo.svg';
import './App.css';
import './style/style.css'
import { Route,Router, Switch } from "react-router-dom"
import Login from './pages/Login';
import Registration from './pages/Registration';
import Homepage from './components/Homepage';

import {createBrowserHistory} from 'history'
import ShowList from './components/Homepage/ShowList';
require('dotenv').config()

function App() {
  return (
    <>
      <Router history={createBrowserHistory()}>
        <Switch>
        <Route exact path="/" component={Login}></Route>
        <Route exact path="/home" component={Homepage}></Route>
        <Route exact path="/login" component={Login}></Route>
        <Route exact path="/registration" component={Registration}></Route>
        <Route exact path="/showlist" component={ShowList}></Route>
        </Switch>
      </Router>
   
    </>
  );
}

export default App;
