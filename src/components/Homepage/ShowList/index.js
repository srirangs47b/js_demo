import React from 'react';
import { useQuery, gql } from '@apollo/client';
import { Spinner, ListGroup, ListGroupItem } from 'reactstrap';


const EXCHANGE_RATES = gql`
  query GetExchangeRates {
    rates(currency: "USD") {
      currency
      rate
    }
  }
`;
const ShowList = () => {

  const { loading, error, data  } = useQuery(EXCHANGE_RATES);

  if (loading) return <Spinner className="loader" color="success" />;
  if (error) return <p>Error :(</p>;

  return data.rates.map(({ currency, rate }) => (
    <div>
    <div key={currency}>
      <ListGroup className="text-center">
        <ListGroupItem color="success">{currency}:{rate}</ListGroupItem>
      </ListGroup>
     
    </div>
    {/* <button onClick={() => refetch()}>Refetch!</button> */}
    </div>
  ));

}

export default ShowList;