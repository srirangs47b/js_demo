import React from 'react';
import Footer from '../../common/Footer';
import Header from '../../common/Header';
import { Container, Row, Col, } from 'reactstrap';
import { gql } from '@apollo/client';
import { client } from '../../index'
import { useDispatch,useSelector } from 'react-redux'



const Homepage = (props) => {
  // const [state, dispatch] = useReducer(reducer, initState);
   const counter = useSelector(state => state.incrementDecrement.counter)
   const dispatch = useDispatch();
   console.log(counter,">>>>>>>>>>>>>>>>>")



  const getData = () => {
    client
      .query({
        query: gql`
        query GetRates {
          rates(currency: "USD") {
            currency
          }
        }
      `
      })
      .then(result => console.log(result));
  }

  return (
    <div>
      
      <Header></Header>
      <Container fluid>
        <Row>
          
          <Col xs={6} id="sidebar-wrapper">
            <p>Welcome from {process.env.REACT_APP_JS_DEMO}</p>
            {getData()}
             <h1 className="text-center">Counter : {counter}</h1>
            <button  onClick={() =>dispatch({type:'INCREMENT'})}>INCREMENT</button>
            <button  onClick={() =>dispatch({type:'DECREMENT'})}>DECREMENT</button>
          </Col>
        </Row>
        
      </Container>
      
      <Footer></Footer>
    </div>
  );
};

export default Homepage;