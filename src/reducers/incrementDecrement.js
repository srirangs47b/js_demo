// import {combineReducers ,createStore} from 'redux'


// const initState = {
//     posts: [
//         {id: 1, title:'React' ,body:'Hooks-redux'},
//         {id: 2, title:'Angular' ,body:'GraphQL'},
//         {id: 3, title:'JavaScript' ,body:'REST-API'}
//     ]
// }

// const combinereducers = combineReducers({
//     posts: []
// })


// const rootReducer = (state = initState , action) =>{
//     return createStore(combinereducers,state);
// }

// export default rootReducer()

const initialState = {
    counter: 0
}

function incrementDecrement (state = initialState , action){
    switch(action.type){
        case 'INCREMENT' :
        return {counter: state.counter + 1}

        case 'DECREMENT' :
        return {counter: state.counter -1}

        default:
            return state;
    }
}

export default incrementDecrement