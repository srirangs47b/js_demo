import React from 'react';


const Footer = () => {
  return (
    <div className="main-footer">
      <div className="container">
        <div className="row">
          {/* Column1 */}
          <div className="col">
            <h4>About</h4>
            <ui className="list-unstyled">
              <li>Contact Us</li>
              <li>Careers</li>
              <li>FAQ</li>
            </ui>
          </div>
          {/* git status */}
          {/* Column2 */}
          <div className="col">
            <h4>Social</h4>
            <ui className="list-unstyled ">
              <li>Facebook</li>
              <li>Instagram</li>
              <li>Twitter</li>
            </ui>
          </div>
          {/* Column3 */}
          <div className="col">
            <h4>Policy</h4>
            <ui className="list-unstyled">
              <li>Return Policy</li>
              <li>Terms Of Use</li>
              <li>Privacy</li>
            </ui>
          </div>
        </div>
        <hr />
        <div className="row text-center">
          <p className="col-sm">
            &copy;{new Date().getFullYear()} 47 Billion | All rights reserved |
            Terms Of Service | Privacy
          </p>
        </div>
      </div>
    </div>
  )
}
export default Footer