import React, { useState } from 'react';
import { Drawer } from 'antd';
import {
  UnorderedListOutlined
} from '@ant-design/icons';

const Sidebar = () => {
  const [visible, setVisible] = useState(false);
  const showDrawer = () => {
    setVisible(true);
  };
  const onClose = () => {
    setVisible(false);
  };
  return (
    <>
      <UnorderedListOutlined onClick={showDrawer} />
      {/* <Button type="primary" onClick={showDrawer}>
        Open
      </Button> */}
      <Drawer
        title="Left sidebar"
        placement="left"
        closable={true}
        onClose={onClose}
        visible={visible}
      >
        <p>Some contents...</p>
        <p>Some contents...</p>
        <p>Some contents...</p>
      </Drawer>
    </>
  );
};

export default Sidebar